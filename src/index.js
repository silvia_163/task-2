$(document).ready(function () {
    // Takes the users data from the JSON file
    $("#usersTable").DataTable({
        ajax: ({
            url: "http://localhost:3000/anagrafica",
            method: "GET",
            dataSrc: "",
            dataType: "json"
        }),
        // Creates the table columns by referring to the JSON file structure
        columns: [
            { data: "idanagrafica" },
            { data: "RagioneSociale" },
            { data: "Indirizzo" },
            { data: "Citta" },
            { data: "Prov" },
            { data: "CAP" },
            { data: "PIVA" },
            { data: "CF" },
            { data: "Telefono" },
            { data: "Fax" },
            { data: "Email" }
        ],
        // Adds the edit/remove buttons to the last column(11)
        columnDefs: [{
            "targets": 11,
            "render": function () {
                return '<button class="edit-button">Modifica</button><button class="remove-button">Rimuovi</button>';
            }
        }]
    });

    // This function allows the form to add new user data to the table
    $("#update-form").on('click', '.add-button', function () {
        // Stores into variables the input fields values that the user has entered
        let id = $("#id-FormInput").val();
        let rs = $("#rs-FormInput").val();
        let adr = $("#adr-FormInput").val();
        let city = $("#city-FormInput").val();
        let pr = $("#pr-FormInput").val();
        let pc = $("#pc-FormInput").val();
        let pv = $("#pv-FormInput").val();
        let tid = $("#tid-FormInput").val();
        let numb = $("#number-FormInput").val();
        let fax = $("#fax-FormInput").val();
        let email = $("#e-FormInput").val();
        
        // Adds a new row with the entered data to the TOP of the table
        if(id != '' && rs != '') {
            $("#usersTable").prepend(
                "<tr>" +
                "<td>" + id + "</td>" +
                "<td>" + rs + "</td>" +
                "<td>" + adr + "</td>" +
                "<td>" + city + "</td>" +
                "<td>" + pr + "</td>" +
                "<td>" + pc + "</td>" +
                "<td>" + pv + "</td>" +
                "<td>" + tid + "</td>" +
                "<td>" + numb + "</td>" +
                "<td>" + fax + "</td>" +
                "<td>" + email + "</td>" +
                "<td>" + "<button class='edit-button'>Modifica</button>" + "<button class='remove-button'>Rimuovi</button>" + "</td>" +
                "</tr>"
                );
            } else {
            alert ("I campi ID e/o Ragione Sociale sono obbligatori ");
        }
     });

    // This function transforms the table cells into input fields
    $("#usersTable").on('click', '.edit-button', function () {
        // For each row, it finds the specific cell with its value (it correspond to the text) and stores it into a variable.
        let idCell = $(this).parents('tr').find('td:eq(0)').text();
        let rsCell = $(this).parents('tr').find('td:eq(1)').text();
        let adrCell = $(this).parents('tr').find('td:eq(2)').text();
        let cityCell = $(this).parents('tr').find('td:eq(3)').text();
        let prCell = $(this).parents('tr').find('td:eq(4)').text();
        let pcCell = $(this).parents('tr').find('td:eq(5)').text();
        let pvCell = $(this).parents('tr').find('td:eq(6)').text();
        let tidCell = $(this).parents('tr').find('td:eq(7)').text();
        let numbCell = $(this).parents('tr').find('td:eq(8)').text();
        let faxCell = $(this).parents('tr').find('td:eq(9)').text();
        let emailCell = $(this).parents('tr').find('td:eq(10)').text();

        /* Each cell of each table row must be transformed into an input field (.html() method).
        *  Then, each input field must be filled with the stored cell values
        */
        $(this).parents('tr').find('td:eq(1)').html("<input id='rs-input' class='form-control form-control-sm' value='" + rsCell + "'>");
        $(this).parents('tr').find('td:eq(2)').html("<input id='adr-input' class='form-control form-control-sm' value='" + adrCell + "'>");
        $(this).parents('tr').find('td:eq(3)').html("<input id='city-input' class='form-control form-control-sm' value='" + cityCell + "'>");
        $(this).parents('tr').find('td:eq(4)').html("<input id='pr-input' class='form-control form-control-sm' value='" + prCell + "'>");
        $(this).parents('tr').find('td:eq(5)').html("<input id='pc-input' class='form-control form-control-sm' value='" + pcCell + "'>");
        $(this).parents('tr').find('td:eq(6)').html("<input id='pv-input' class='form-control form-control-sm' value='" + pvCell + "'>");
        $(this).parents('tr').find('td:eq(7)').html("<input id='tid-input' class='form-control form-control-sm' value='" + tidCell + "'>");
        $(this).parents('tr').find('td:eq(8)').html("<input id='numb-input' class='form-control form-control-sm' value='" + numbCell + "'>");
        $(this).parents('tr').find('td:eq(9)').html("<input id='fax-input' class='form-control form-control-sm' value='" + faxCell + "'>");
        $(this).parents('tr').find('td:eq(10)').html("<input id='email-input' class='form-control form-control-sm' value='" + emailCell + "'>");

        /* When the user clicks on the "Edit button", an "Update" button will be applied to the 11° column of the table.
         * At the same time, the "Edit button" will be hidden
        */
        $(this).parents('tr').find('td:eq(11)').prepend('<button class="update-button">Aggiorna</button>');
        $(this).hide();
    });

    // This function updates the user data through the "Edit button"
    $("#usersTable").on('click', '.update-button', function () {
        // Stores the values of the table input fields that the user has eventually filled with new data
        let idInput = $(this).parents('tr').find('#id-input').val();
        let rsInput = $(this).parents('tr').find('#rs-input').val();
        let adrInput = $(this).parents('tr').find('#adr-input').val();
        let cityInput = $(this).parents('tr').find('#city-input').val();
        let prInput = $(this).parents('tr').find('#pr-input').val();
        let pcInput = $(this).parents('tr').find('#pc-input').val();
        let pvInput = $(this).parents('tr').find('#pv-input').val();
        let tidInput = $(this).parents('tr').find('#tid-input').val();
        let numbInput = $(this).parents('tr').find('#numb-input').val();
        let faxInput = $(this).parents('tr').find('#fax-input').val();
        let emailInput = $(this).parents('tr').find('#email-input').val();

        // For each row, it finds the specific cell and fill it with the stored data
        $(this).parents('tr').find('td:eq(0)').text(idInput);
        $(this).parents('tr').find('td:eq(1)').text(rsInput);
        $(this).parents('tr').find('td:eq(2)').text(adrInput);
        $(this).parents('tr').find('td:eq(3)').text(cityInput);
        $(this).parents('tr').find('td:eq(4)').text(prInput);
        $(this).parents('tr').find('td:eq(5)').text(pcInput);
        $(this).parents('tr').find('td:eq(6)').text(pvInput);
        $(this).parents('tr').find('td:eq(7)').text(tidInput);
        $(this).parents('tr').find('td:eq(8)').text(numbInput);
        $(this).parents('tr').find('td:eq(9)').text(faxInput);
        $(this).parents('tr').find('td:eq(10)').text(emailInput);

        /* When the user click on the "Update button", instead, the "Edit button" is shown again. 
        *  At the same time, the "Update button" itself is hidden
        */
        $(".edit-button").show();
        $('.update-button').hide();
    });

    // This function removes one row from the table
    $("#usersTable").on('click', '.remove-button', function () {
        $(this).parents('tr').remove();
    });
    
    // This function allows to move to the top of the page when a new user row is added
    $(".add-button").click(function() {
        $(window).scrollTop(0);
    });
});